﻿using Microsoft.EntityFrameworkCore;
using Api.Vendas.Entities;
using Api.Vendas.Models;

namespace Api.Vendas.Context;

public class SeguradoraContext : DbContext
{
    public SeguradoraContext(DbContextOptions<SeguradoraContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Produto>()
            .HasMany(p => p.Vendas)
            .WithOne(p => p.Produto)
            .HasForeignKey(p => p.ProdutoId);

        modelBuilder.Entity<Vendedor>()
            .HasMany(v => v.Vendas)
            .WithOne(v => v.Vendedor)
            .HasForeignKey(v => v.VendedorId);

        modelBuilder.Seed();
    }

    public DbSet<Produto> Produtos { get; set; }
    public DbSet<Vendedor> Vendedores { get; set; }
    public DbSet<Venda> Vendas { get; set; }
}
