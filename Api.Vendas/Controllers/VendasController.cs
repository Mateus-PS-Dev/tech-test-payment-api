﻿using Microsoft.AspNetCore.Mvc;
using Api.Vendas.Context;
using Api.Vendas.Entities;
using Api.Vendas.Models;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;

namespace Api.Vendas.Controllers;

[Route("api/[controller]")]
[ApiController]
public class VendasController : ControllerBase
{
    private readonly SeguradoraContext _context;

    public VendasController(SeguradoraContext context)
    {
        _context = context;

        _context.Database.EnsureCreated();
    }

    /// <summary>
    /// Este serviço permite fazer a busca de uma Venda.
    /// </summary>
    /// <param name="id">Busca uma Venda pelo o Id</param>
    /// <returns>Retorna status Ok mais os dados do usuario e NotFound  e o erro</returns>
    [SwaggerResponse(
        statusCode: 200,
        description: "Sucesso ao obter uma Venda",
        type: typeof(Venda))]
    [SwaggerResponse(
        statusCode: 404,
        description: "Requisição não encontrado",
        type: typeof(NotFoundResult))]
    [HttpGet]
    [Route("[action]")]
    public async Task<ActionResult> BuscarVenda([Required] int id)
    {
        var venda = await _context.Vendas.FindAsync(id);

        if (venda == null)
            return NotFound(new
            {
                Erro = $"Venda de Id {id} não encontrado."
            });

        return Ok(venda);
    }

    /// <summary>
    /// Este serviço permite adicionar uma venda.
    /// </summary>
    /// <param name="vendedorId">Id do Vendedor que está a realizar a venda</param>
    /// <param name="produtoId">Id do Produto a ser vendido</param>
    /// <returns></returns>
    [SwaggerResponse(
        statusCode: 201,
        description: "Sucesso ao cadastrar uma Venda",
        type: typeof(Venda))]
    [SwaggerResponse(
        statusCode: 400,
        description: "Campos obrigatórios",
        type: typeof(NotFoundResult))]
    [SwaggerResponse(
        statusCode: 404,
        description: "Requisição não encontrado",
        type: typeof(NotFoundResult))]
    [HttpPost]
    [Route("[action]")]
    public async Task<IActionResult> RegistrarVenda([Required] int vendedorId, [Required] int produtoId)
    {
        Venda venda = new Venda();

        var vendedor = await _context.Vendedores.FindAsync(vendedorId);
        var produto = await _context.Produtos.FindAsync(produtoId);

        if (vendedor == null)
            return NotFound(new
            {
                Erro = $"Vendedor de Id {vendedorId} não encontrado."
            });

        else if (produto == null)
            return NotFound(new
            {
                Erro = $"Produto de Id {produtoId} não encontrado."
            });

        else if (!ModelState.IsValid)
            return BadRequest();

        venda.Status = StatusVenda.AguardandoPagamento;
        venda.DataVenda = DateTime.Now;
        venda.VendedorId = vendedor.Id;
        venda.ProdutoId = produto.Id;

        _context.Vendas.Add(venda);
        await _context.SaveChangesAsync();

        return CreatedAtAction(
            nameof(BuscarVenda),
            new { id = venda.Id },
            venda);
    }

    /// <summary>
    /// Este serviço permite a modifição do status da Venda.
    /// </summary>
    /// <param name="id">Id da Venda para atualização</param>
    /// <param name="status">Status para atualização</param>
    /// <returns>
    /// Retorna NoContente Vazio, NotFound caso não achar a Venda e Bad Requeste em caso de mudaça não autroizada
    /// </returns>
    [SwaggerResponse(
        statusCode: 200,
        description: "Sucesso ao atualizar a Venda",
        type: typeof(Venda))]
    [SwaggerResponse(
        statusCode: 400,
        description: "Campos obrigatórios",
        type: typeof(NotFoundResult))]
    [SwaggerResponse(
        statusCode: 404,
        description: "Requisição não encontrado",
        type: typeof(NotFoundResult))]
    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> AtualizarVenda([Required] int id, [Required] StatusVenda status)
    {
        var vendaDb = await _context.Vendas.FindAsync(id);

        if (vendaDb == null)
            return NotFound(new
            {
                Erro = $"Venda de Id {id} não encontrado."
            });

        if (ValidaStatus.Validar(vendaDb, status))
            return BadRequest(new
            {
                erro = $"Não é válida a mudança de Status {vendaDb.Status} para {status}."
            });

        vendaDb.Status = status;

        await _context.SaveChangesAsync();

        return NoContent();
    }
}
