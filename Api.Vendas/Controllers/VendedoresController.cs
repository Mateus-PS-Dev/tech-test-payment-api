﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Api.Vendas.Context;
using Swashbuckle.AspNetCore.Annotations;
using Api.Vendas.Entities;

namespace Api.Vendas.Controllers;

[Route("[controller]")]
[ApiController]
public class VendedoresController : ControllerBase
{
    private readonly SeguradoraContext _context;

    public VendedoresController(SeguradoraContext context)
    {
        _context = context;

        _context.Database.EnsureCreated();
    }

    /// <summary>
    /// Este serviço exibe todos os Vendedores em memória.
    /// </summary>
    /// <returns>Retorna status Ok com todos Venddedor</returns>
    [SwaggerResponse(
        statusCode: 200,
        description: "Sucesso ao obter os Vendedores",
        type: typeof(List<Vendedor>))]
    [HttpGet]
    [Route("[action]")]
    public async Task<ActionResult> BuscarTodosVendedores()
    {
        var vendedores = await _context.Vendedores.ToListAsync();

        return Ok(vendedores);
    }
}
