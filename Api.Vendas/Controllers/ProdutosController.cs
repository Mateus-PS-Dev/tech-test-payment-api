﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Api.Vendas.Context;
using Swashbuckle.AspNetCore.Annotations;
using Api.Vendas.Entities;

namespace Api.Vendas.Controllers;

[Route("[controller]")]
[ApiController]
public class ProdutosController : ControllerBase
{
    private readonly SeguradoraContext _context;

    public ProdutosController(SeguradoraContext context)
    {
        _context = context;

        _context.Database.EnsureCreated();
    }

    /// <summary>
    /// Este serviço exibe todos os Produtos em memória.
    /// </summary>
    /// <returns>Retorna status Ok com todos Produtos</returns>
    [SwaggerResponse(
        statusCode: 200,
        description: "Sucesso ao obter os Produtos",
        type: typeof(List<Produto>))]
    [Route("[action]")]
    [HttpGet]
    public async Task<ActionResult> BuscarTodosProdutos()
    {
        var produtos = await _context.Produtos.ToListAsync();

        return Ok(produtos);
    }
}
