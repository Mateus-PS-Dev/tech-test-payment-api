﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Api.Vendas.Entities;

public class Venda
{
    public int Id { get; set; }
    public StatusVenda Status { get; set; }
    public DateTime DataVenda { get; set; }

    [JsonIgnore]
    public virtual Vendedor? Vendedor { get; set; }
    public int VendedorId { get; set; }

    [JsonIgnore]
    public virtual Produto? Produto { get; set; }
    public int ProdutoId { get; set; }
}

public enum StatusVenda
{
    AguardandoPagamento = 0,
    PagamentoAprovado = 1,
    EnviadoTransportadora = 2,
    Entregue = 3,
    Cancelada = 4
}
