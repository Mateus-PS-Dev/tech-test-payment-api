﻿using System.Text.Json.Serialization;

namespace Api.Vendas.Entities;

public class Vendedor
{
    public int Id { get; set; }
    public string Cpf { get; set; }
    public string Nome { get; set; }
    public string Telefone { get; set; }
    public string Email { get; set; }

    [JsonIgnore]
    public virtual List<Venda> Vendas { get; set; }
}
