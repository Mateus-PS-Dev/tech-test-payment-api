﻿using System.Text.Json.Serialization;

namespace Api.Vendas.Entities;

public class Produto
{
    public int Id { get; set; }
    public string Nome { get; set; }
    public decimal Preco { get; set; }

    [JsonIgnore]
    public virtual List<Venda> Vendas { get; set; }
}
