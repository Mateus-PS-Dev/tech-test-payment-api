﻿using Microsoft.EntityFrameworkCore;
using Api.Vendas.Entities;

namespace Api.Vendas.Models;

public static class ModelBuilderExtensions
{
    public static void Seed (this ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Venda>().HasData(
            new Venda
            {
                Id = 1,
                Status = StatusVenda.AguardandoPagamento,
                DataVenda = DateTime.Now,
                VendedorId = 1,
                ProdutoId = 1
            },
            new Venda
            {
                Id = 2,
                Status = StatusVenda.PagamentoAprovado,
                DataVenda = DateTime.Now,
                VendedorId = 1,
                ProdutoId = 2
            },
            new Venda
            {
                Id = 3,
                Status = StatusVenda.EnviadoTransportadora,
                DataVenda = DateTime.Now,
                VendedorId = 1,
                ProdutoId = 3
            },
            new Venda
            {
                Id = 4,
                Status = StatusVenda.Entregue,
                DataVenda = DateTime.Now,
                VendedorId = 2,
                ProdutoId = 4
            },
            new Venda
            {
                Id = 5,
                Status = StatusVenda.Cancelada,
                DataVenda = DateTime.Now,
                VendedorId = 3,
                ProdutoId = 5
            });


        modelBuilder.Entity<Vendedor>().HasData(
            new Vendedor
            {
                Id = 1,
                Nome = "Mateus Pereira Silva",
                Cpf = "11111111111",
                Telefone = "(11) 91111 1111",
                Email = "mateuspsdev@apivenda.com"
            },
            new Vendedor
            {
                Id = 2,
                Nome = "Fulano de Tal",
                Cpf = "22222222222",
                Telefone = "(21) 92222 2222",
                Email = "fulano@apivenda.com"
            },
            new Vendedor
            {
                Id = 3,
                Nome = "Sicrano de Tal",
                Cpf = "333.333.333-33",
                Telefone = "(31) 93333 3333",
                Email = "sicrano@apivenda.com"
            });
        
        modelBuilder.Entity<Produto>().HasData(
            new Produto
            {
                Id = 1,
                Nome = "Seguro Casa até 150M2 - Anual",
                Preco = 1000.00M
            },
            new Produto
            {
                Id = 2,
                Nome = "Seguro Casa até 500M2 - Anual",
                Preco = 1750.00M
            },
            new Produto
            {
                Id = 3,
                Nome = "Seguro Carro Popular, Para Mulher com até 25anos - Anual",
                Preco = 890.00M
            },
            new Produto
            {
                Id = 4,
                Nome = "Seguro Carro Popular, Para Homem com até 25anos - Anual",
                Preco = 990.00M
            },
            new Produto
            {
                Id = 5,
                Nome = "Seguro Carro Popular, Para Mulher com acima 25anos - Anual",
                Preco = 680.00M
            },
            new Produto
            {
                Id = 6,
                Nome = "Seguro Carro Popular, Para Homem com acima 25anos - Anual",
                Preco = 680.00M
            });
    }
}
