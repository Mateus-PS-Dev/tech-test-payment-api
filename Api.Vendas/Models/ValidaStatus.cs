﻿using Api.Vendas.Entities;

namespace Api.Vendas.Models;

public class ValidaStatus
{
    public static bool Validar(Venda venda, StatusVenda status)
    {
        switch (venda.Status.GetHashCode())
        {
            case 0:
                if ((status.HasFlag(StatusVenda.PagamentoAprovado) ||
                    status.HasFlag(StatusVenda.Cancelada)) &&
                    !status.HasFlag(StatusVenda.Entregue))
                {
                    return false;
                }
                else
                    return true;

            case 1:
                if ((status.HasFlag(StatusVenda.EnviadoTransportadora) ||
                    status.HasFlag(StatusVenda.Cancelada)) &&
                    !status.HasFlag(StatusVenda.Entregue))
                {
                    return false;
                }
                else
                    return true;

            case 2:
                if (status.HasFlag(StatusVenda.Entregue))
                {
                    return false;
                }
                else
                    return true;

            case 3:
                if (status.HasFlag(StatusVenda.Entregue))
                {
                    return true;
                }
                return true;

            case 4:
                if (status.HasFlag(StatusVenda.Cancelada))
                {
                    return true;
                }
                return true;
            default:
                return true;
        }
    }
}