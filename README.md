# Api.Venda
## CONCLUSÃO DO PROJETO:
Assim como proposto, essa é uma Api REST de venda só para venda de seguros.

- A venda é validade com a entrada do Vendedor que está efetuando a venda de um único Produto;
- A busca é efetuada através do Id da venda;
- Atualizações são realizada somente no Status da Venda;

- **Nota**: A Api foi desenvolvida em memória, sem permanência em banco de dados, com algumas instâncias das entidades já realizada no código.
  - [Arquivo de consulta das entidades em memória.](https://gitlab.com/Mateus-PS-Dev/tech-test-payment-api/-/blob/main/Arquivos/dados-em-memoria.md) 


### TABELA DE MÉTODOS HTTP:

| Verbos HTTP | Entradas |  Retornos |
| :--- | :--- | :--- |
|_**Registrar Venda<br> (POST)**_ |  * *int* **Id do Vendedor** - Parâmetro Requerido;<br>* *int* **Id do Produto** - Parâmetro Requerido;<br>* *Enum* **Status** - Definido Como Aguardando Pagamento;<br>* *DataTime* **Data Venda** - Definido como Hora atual no momento que gerada a venda.|* *Created At Action	201*,<br>* *Not Found 404*<br>* *BadRequest	400*
|_**Buscar Venda<br> (GET)**_ | * *int* **Id da Venda**. |* *Ok 200*<br>* *Not Found 404*
|_**Atualizar Venda<br> (PUT)**_ | * *int* **Id da Venda**;<br> * *Enum* **Status**. |* *No Content 204*<br>* *Not Found 404*<br>* *Bad Request 400*|
|* *Buscar Todos Produtos*<br>* *Buscar Todos Vendedores*<br>_**(GET)**_ | *VAZIO* ( Somente para consulta dos vendedores e produtos que estão na memória ) | *Ok 200*
   
## Considerações Finais:

- **Primeiramente agradecer a Pottencial e a DIO pela oportunidade, esse curso foi excelente, me trouxe muito conhecimento novo. Muito Bom!!🧡** 

- Como é uma venda de seguros talvez os Status não encaixe muito bem para idéia de negócio, mas não alterei para alterar uma parte das exigências, mas tive algumas ideias como ficaria melhor, por exemplo:
  - `Aguardando pagamento = Aguardando aceitação contrato`
  - `Pagamento aprovado = Contrato aceito`
  - `Enviado transportadora = Aguardando liberação seguradora`
  - `Entregue = Seguro Ativo` (Aqui um outro DateTime)
  - `Cancelada`

- Não foi realizado muitos commits no projeto, pois está adquirindo outros conhecimentos para tentar fazer com que a Api funcionasse de forma mais apropriada, e durante o processo teve muita tentativa e erro, muita mesmo;
- Espero ter atendido todo os requisitos.
___
## INSTRUÇÕES PARA O TESTE TÉCNICO

- Crie um fork deste projeto (https://gitlab.com/Pottencial/tech-test-payment-api/-/forks/new). É preciso estar logado na sua conta Gitlab;
- Adicione @Pottencial (Pottencial Seguradora) como membro do seu fork. Você pode fazer isto em  https://gitlab.com/`your-user`/tech-test-payment-api/settings/members;
 - Quando você começar, faça um commit vazio com a mensagem "Iniciando o teste de tecnologia" e quando terminar, faça o commit com uma mensagem "Finalizado o teste de tecnologia";
 - Commit após cada ciclo de refatoração pelo menos;
 - Não use branches;
 - Você deve prover evidências suficientes de que sua solução está completa indicando, no mínimo, que ela funciona;

## O TESTE
- Construir uma API REST utilizando .Net Core, Java ou NodeJs (com Typescript);
- A API deve expor uma rota com documentação swagger (http://.../api-docs).
- A API deve possuir 3 operações:
  1) Registrar venda: Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento";
  2) Buscar venda: Busca pelo Id da venda;
  3) Atualizar venda: Permite que seja atualizado o status da venda.
     * OBS.: Possíveis status: `Pagamento aprovado` | `Enviado para transportadora` | `Entregue` | `Cancelada`.
- Uma venda contém informação sobre o vendedor que a efetivou, data, identificador do pedido e os itens que foram vendidos;
- O vendedor deve possuir id, cpf, nome, e-mail e telefone;
- A inclusão de uma venda deve possuir pelo menos 1 item;
- A atualização de status deve permitir somente as seguintes transições: 
  - De: `Aguardando pagamento` Para: `Pagamento Aprovado`
  - De: `Aguardando pagamento` Para: `Cancelada`
  - De: `Pagamento Aprovado` Para: `Enviado para Transportadora`
  - De: `Pagamento Aprovado` Para: `Cancelada`
  - De: `Enviado para Transportador`. Para: `Entregue`
- A API não precisa ter mecanismos de autenticação/autorização;
- A aplicação não precisa implementar os mecanismos de persistência em um banco de dados, eles podem ser persistidos "em memória".

## PONTOS QUE SERÃO AVALIADOS
- Arquitetura da aplicação - embora não existam muitos requisitos de negócio, iremos avaliar como o projeto foi estruturada, bem como camadas e suas responsabilidades;
- Programação orientada a objetos;
- Boas práticas e princípios como SOLID, DDD (opcional), DRY, KISS;
- Testes unitários;
- Uso correto do padrão REST.
