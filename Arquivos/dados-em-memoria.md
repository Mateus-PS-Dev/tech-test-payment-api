# DADOS EM MEMÓRIA:

## VENDAS:
```json
[
    {
      "id": 1,
      "status": "AguardandoPagamento",
      "dataVenda": "2022-11-09T16:16:45.4834814-03:00",
      "vendedorId": 1,
      "produtoId": 1
    },
    {
      "id": 2,
      "status": "PagamentoAprovado",
      "dataVenda": "2022-11-09T16:16:45.4834827-03:00",
      "vendedorId": 1,
      "produtoId": 2
    },
    {
      "id": 3,
      "status": "EnviadoTransportadora",
      "dataVenda": "2022-11-09T16:16:45.4834828-03:00",
      "vendedorId": 1,
      "produtoId": 3
    },
    {
      "id": 4,
      "status": "Entregue",
      "dataVenda": "2022-11-09T16:16:45.4834829-03:00",
      "vendedorId": 2,
      "produtoId": 4
    },
    {
      "id": 5,
      "status": "Cancelada",
      "dataVenda": "2022-11-09T16:16:45.483483-03:00",
      "vendedorId": 3,
      "produtoId": 5
    }
]
```
## Vendedores:
```json
[
  {
    "id": 1,
    "cpf": "11111111111",
    "nome": "Mateus Pereira Silva",
    "telefone": "(11) 91111 1111",
    "email": "mateuspsdev@apivenda.com",
    "vendas": null
  },
  {
    "id": 2,
    "cpf": "22222222222",
    "nome": "Fulano de Tal",
    "telefone": "(21) 92222 2222",
    "email": "fulano@apivenda.com",
    "vendas": null
  },
  {
    "id": 3,
    "cpf": "333.333.333-33",
    "nome": "Sicrano de Tal",
    "telefone": "(31) 93333 3333",
    "email": "sicrano@apivenda.com",
    "vendas": null
  }
]
```
## Produtos:
```json
[
  {
    "id": 1,
    "nome": "Seguro Casa até 150M2 - Anual",
    "preco": 1000,
    "vendas": null
  },
  {
    "id": 2,
    "nome": "Seguro Casa até 500M2 - Anual",
    "preco": 1750,
    "vendas": null
  },
  {
    "id": 3,
    "nome": "Seguro Carro Popular, Para Mulher com até 25anos - Anual",
    "preco": 890,
    "vendas": null
  },
  {
    "id": 4,
    "nome": "Seguro Carro Popular, Para Homem com até 25anos - Anual",
    "preco": 990,
    "vendas": null
  },
  {
    "id": 5,
    "nome": "Seguro Carro Popular, Para Mulher com acima 25anos - Anual",
    "preco": 680,
    "vendas": null
  },
  {
    "id": 6,
    "nome": "Seguro Carro Popular, Para Homem com acima 25anos - Anual",
    "preco": 680,
    "vendas": null
  }
]
```